-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vytvořeno: Sob 09. dub 2022, 15:05
-- Verze serveru: 5.7.11
-- Verze PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `eshop`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(60) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`) VALUES
(1, 'Theater'),
(2, 'Concert');

-- --------------------------------------------------------

--
-- Struktura tabulky `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `total` decimal(10,3) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `orders`
--

INSERT INTO `orders` (`order_id`, `date`, `total`, `user_id`, `status_id`) VALUES
(11, '2022-04-09 16:55:09', '998.000', 4, 1),
(12, '2022-04-09 16:55:23', '2997.000', 2, 2),
(13, '2022-04-09 16:55:43', '3000.000', 2, 1),
(14, '2022-04-09 16:55:59', '697.000', 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `order_content`
--

CREATE TABLE `order_content` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `order_content`
--

INSERT INTO `order_content` (`id`, `order_id`, `product_id`, `order_amount`) VALUES
(23, 11, 10, 1),
(24, 11, 9, 1),
(25, 11, 11, 1),
(26, 12, 14, 3),
(27, 13, 15, 1),
(28, 13, 13, 2),
(29, 13, 12, 2),
(30, 14, 11, 1),
(31, 14, 10, 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(120) COLLATE utf8_czech_ci NOT NULL,
  `product_price` decimal(10,3) NOT NULL,
  `product_description` longtext COLLATE utf8_czech_ci NOT NULL,
  `amount_in_stock` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_price`, `product_description`, `amount_in_stock`, `category_id`) VALUES
(9, 'Miška', '500.000', 'Koncert Miška je velice zajímavý koncert plný kocertových kocertů.', 40, 2),
(10, 'R. U. R', '199.000', 'Etiam bibendum elit eget erat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris tincidunt sem sed arcu. Etiam posuere lacus quis dolor. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Maecenas libero. Duis viverra diam non justo. Nulla non arcu lacinia neque faucibus fringilla. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Duis pulvinar.', 20, 1),
(11, 'Romeo a Julie', '299.000', 'Dva mladičtí veronští milenci jsou díky Williamu Shakespearovi (1564–1616) snad nejslavnějším mileneckým párem evropské kultury; znají je i ti, kdo tragédii slavného Alžbětince nikdy neviděli ani nečetli. Hra je také snad největší oslavou lásky, lidského citu, který je se vší svou bezbrannou zranitelností schopen postavit se předsudkům, rodové zášti, intrikám, posměchu a nenávisti.\n\nVelké činoherní divadlo by se mělo k této hře pravidelně vracet a zprostředkovat vždy novým generacím diváků onen prožitek citu – a starším generacím jej připomenout. Příběh, plný nebezpečných a smrtících pouličních bitek, ale také citových vyznání, se slavnou balkonovou scénou, s nezapomenutelným Mercuziovým monologem o královně snů Mab, s mileneckou hádkou po noci lásky, zda ranní pěvec, který je budí, je slavík, či skřivan, i s předstíranou sebevraždou, která se vymkne v tragický závěr hry, se dočkal řady zpracování – také ve filmu, opeře, baletu, muzikálu. Neuškodí proto vrátit se k jeho původní podobě, jak ji napsal dodnes nejhranější evropský autor před čtyřmi sty dvaceti lety – a v současném překladu Jiřího Joska nechat zaznít bez dekorativních modernizací silný příběh v jeho základní jevištní účinnosti.\n\nRomeo a Julie je pro inscenátory i diváky vzácnou příležitostí vrátit se do světa poezie, představivosti a hlubokého citu, hodnot, které dnes tak často postrádáme, a které se ve vrcholné dramatické básni utkají s fyzickým násilím, pragmatismem, intrikami a propastnou nenávistí. Takový dramatický konflikt obou krajních poloh lidského bytí na tomto světě nás může obdařit novým pochopením vyššího smyslu lásky v našem každodenním životě.', 30, 1),
(12, 'Je třeba zabít Sekala', '350.000', 'Je horké léto roku 1943. Jura Baran (Tomáš Dastlík) pomáhal partyzánům a teď se přichází do vesnice Lakotice ukrýt před Němci. Ale i uvnitř vsi zuří válka. Sekal (Marek Holý), který byl vychováván jako parchant, se mstí svým sousedům. Donáší Němcům, pomalu si bere grunt za gruntem a pantáty posílá do koncentráku. Rigidní řád majitelů gruntů se obrátil proti nim samým. Rada starších se proto usnese: Je třeba zabít Sekala. Ale kdo to má udělat? A jak chtějí vyrvat zlo, které koření z půdy, kterou sami oseli? Jura Baran je pro ně jen přivandrovalec, luterán, který by se dal využít. Jediným přítelem se mu stane katolický kněz, Páter Flóra (Daniel Bambas). Ale může mu věřit? Postaví se za luterána? A existuje cesta ven ze začarovaného kruhu násilí?\n\nJiří Křižan napsal Je třeba zabít Sekala jako filmový scénář. Námět je to ale klasicky divadelní, jak potvrzuje úspěšná adaptace Martina Františáka. Pantátové dali vzniknout Sekalovi skrze ponižování, ale teď se sami stávají podřízenými v totalitě zrozené z pomsty. Je třeba zabít Sekala vypovídá o vlivu totality na jedince i na společnost. Zároveň se v tomto syrovém českém westernu ukrývají zásadní otázky, týkající se povahy dobra a zla. Kdo a co přežije? Jevištní zpracování scénáře Jiřího Křižana, podle kterého vznikl slavný český film, je dílem Martina Františáka.', 20, 1),
(13, 'Miláček', '400.000', 'Paříž konce 19. století je zářivou perlou evropských metropolí, plnou kabaretů, divadel, kaváren, hospůdek, barů, společenské zábavy a vzrušeného nočního života. Ovšem především pro ty, kdo na to mají. George Duroy k nim nepatří. Nedávno se vrátil z armádní služby v Africe, má špatně placenou práci, žádné vzdělání ani perspektivu, věčně hluboko do kapsy. Jednoho šťastného večera potká starého známého z armády Forestiera, s jehož pomocí se dostane k práci v novinách. A díky své vůli a především své schopnosti zaujmout ženy začne stoupat po společenském žebříčku. A začíná jeho dechberoucí jízda pařížskou společností směrem k vrcholu…\nPříběh vzestupu George Duroye je starý více než sto let, ale svým způsobem stále živý a aktuální. Není pouze záznamem o vzestupu krásného a sympatického, leč bezcharakterního jedince a jeho lehce pikantních milostných dobrodružstvích, která tuto cestu vzhůru provázejí. Je také zobrazením světa, v němž se nebezpečným způsobem rozevírají sociální nůžky, v němž mají média obrovskou moc, ovlivňují politické procesy i veřejné mínění a dochází k jejich propojení s vládnoucími strukturami. Je zobrazením světa, který se v mnohém liší, ale zároveň v mnohém připomíná svět, ve kterém žijeme dnes.', 50, 1),
(14, 'ONEREPUBLIC', '999.000', 'Mauris tincidunt sem sed arcu. Aliquam id dolor. Aliquam erat volutpat. Aliquam id dolor. Integer vulputate sem a nibh rutrum consequat. In rutrum. Pellentesque sapien. Suspendisse sagittis ultrices augue. Mauris dolor felis, sagittis at, luctus sed, aliquam non, tellus. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor.\n\nAliquam in lorem sit amet leo accumsan lacinia. Nunc dapibus tortor vel mi dapibus sollicitudin. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Nam quis nulla. Duis pulvinar. Nunc dapibus tortor vel mi dapibus sollicitudin. Praesent dapibus. Et harum quidem rerum facilis est et expedita distinctio. Nullam at arcu a est sollicitudin euismod. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus rhoncus. Nunc dapibus tortor vel mi dapibus sollicitudin. Aliquam erat volutpat. Praesent vitae arcu tempor neque lacinia pretium.', 50, 2),
(15, 'Wohnout', '1500.000', '29.12.2065\nMauris tincidunt sem sed arcu. Aliquam id dolor. Aliquam erat volutpat. Aliquam id dolor. Integer vulputate sem a nibh rutrum consequat. In rutrum. Pellentesque sapien. Suspendisse sagittis ultrices augue. Mauris dolor felis, sagittis at, luctus sed, aliquam non, tellus. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor.\n\nAliquam in lorem sit amet leo accumsan lacinia. Nunc dapibus tortor vel mi dapibus sollicitudin. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Nam quis nulla. Duis pulvinar. Nunc dapibus tortor vel mi dapibus sollicitudin. Praesent dapibus. Et harum quidem rerum facilis est et expedita distinctio. Nullam at arcu a est sollicitudin euismod. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus rhoncus. Nunc dapibus tortor vel mi dapibus sollicitudin. Aliquam erat volutpat. Praesent vitae arcu tempor neque lacinia pretium.', 20, 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(30) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'admin'),
(2, 'user'),
(3, 'asd');

-- --------------------------------------------------------

--
-- Struktura tabulky `status_list`
--

CREATE TABLE `status_list` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(32) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `status_list`
--

INSERT INTO `status_list` (`status_id`, `status_name`) VALUES
(1, 'Created'),
(2, 'Completed');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `firstname` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `lastname` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `firstname`, `lastname`, `email`, `role_id`) VALUES
(1, 'admin', '$2y$12$nNm7tYLaj60LXw5hDO1Ay.EDoTPMGsa3aIahH1AC7XTlhcpN.wR8K', 'admin', 'admin', 'admin@eshop.com', 1),
(2, 'user', '$2a$12$byI2wA1jOoIIItToyig4Xuw...qQG.ph3yyw/3G/X.IFOTqbcfvui', 'Userovskej', 'Userík', 'user@eshop.com', 2),
(4, 'root', '$2y$12$kTUSkahGjIu1f8n6x1vf0.cosFYWnB8glZHudjerE9TybAeqXMJha', 'gqgq', 'asd', 'vrata@dostodoly.cz', 1);

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Klíče pro tabulku `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Klíče pro tabulku `order_content`
--
ALTER TABLE `order_content`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Klíče pro tabulku `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Klíče pro tabulku `status_list`
--
ALTER TABLE `status_list`
  ADD PRIMARY KEY (`status_id`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pro tabulku `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pro tabulku `order_content`
--
ALTER TABLE `order_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pro tabulku `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pro tabulku `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pro tabulku `status_list`
--
ALTER TABLE `status_list`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
