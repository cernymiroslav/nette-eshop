<?php
namespace App\Forms;
use Nette\Application\UI\Form;

final class ChangePasswordFactory {    
    /**
     * creates change password form
     *
     * @return Form change password form
     */
    function create(): Form {
        $form = new Form;
        
        $form->addPassword('current_password', 'Current password:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addPassword('new_password', 'New password:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addPassword('new_password_repeat', 'New password again:')
        ->setRequired(('Vyplňte prosím %label'))
        ->addRule($form::EQUAL, 'Hesla se neshodují', $form['new_password'])
        ->setOmitted();
        
        $form->addSubmit('send', 'Change password');
        //$form->onSuccess[] = [$this, 'formSucceeded']; //nutno dát tam, kde se bude používat, a vytvořit metodu

		return $form;
    }

}



?>