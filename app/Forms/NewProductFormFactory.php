<?php
namespace App\Forms;
use Nette\Application\UI\Form;

final class NewProductFormFactory {
    
        
    /**
     * creates new product form
     *
     * @param  array $categories array of category objects
     * @return Form new product form
     */
    function create($categories): Form {
        $form = new Form;
        $form->addText('product_name', 'Název produktu:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addTextArea('product_description', 'Popis produktu:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addText('product_price', 'Cena produktu[float]:')
        ->setRequired(('Vyplňte prosím %label'))
        ->addRule(Form::FLOAT, 'Zadejte prosím do %label číslo')
        ->setNullable();

        $form->addInteger('amount_in_stock', 'Počet na skladě[int]:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addSelect('category_id', 'Kategorie[ID]:', $categories);


        $form->addSubmit('send', 'Odeslat');
        //$form->onSuccess[] = [$this, 'formSucceeded']; //nutno dát tam, kde se bude používat, a vytvořit metodu

		return $form;
    }

}



?>