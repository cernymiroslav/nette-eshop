<?php
namespace App\Forms;
use Nette\Application\UI\Form;

final class SignInFactory {    
    /**
     * creates sign in form
     *
     * @return Form sign in form
     */
    function create(): Form {
        $form = new Form;
        
        $form->addText('email', 'Email:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addText('firstname', 'Jméno:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addText('surname', 'Příjmení:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addPassword('password', 'Heslo:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addPassword('passwordRepeat', 'Heslo znovu:')
        ->setRequired(('Vyplňte prosím %label'));
        $form->addSubmit('send', 'Registrovat');
       //$form->onSuccess[] = [$this, 'formSucceeded']; //nutno dát tam, kde se bude používat, a vytvořit metodu

		return $form;
    }

}



?>