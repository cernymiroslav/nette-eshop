<?php
namespace App\Forms;
use Nette\Application\UI\Form;

final class CalculatorFactory {
        
    /**
     * creates calculator form
     *
     * @return Form calculator form
     */
    function create(): Form {
        $form = new Form;
		
        $znaminka = [
            '+' => '+',
            '-' => '-',
            '*' => '*',
            '/' => '/'
        ];
        
        $form->addInteger('cislo1', '1. číslo:');
        $form->addSelect('znaminka', 'Znamínko', $znaminka);
		$form->addInteger('cislo2', '2. číslo:');
        $form->addSubmit('send', 'Výpočet');



		return $form;
    }

}



?>