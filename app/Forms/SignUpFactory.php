<?php
namespace App\Forms;
use Nette\Application\UI\Form;

final class SignUpFactory {    
    /**
     * creates sign up form
     *
     * @return Form sign up form
     */
    function create(): Form {
        $form = new Form;
        
        $form->addText('email', 'Email:')
        ->setRequired(('Vyplňte prosím %label'))
        ->setHtmlAttribute('placeholder', 'email@cotijedotoho.cz');

        $form->addText('username', 'Username:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addText('firstname', 'Jméno:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addText('surname', 'Příjmení:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addPassword('password', 'Heslo:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addPassword('password_repeat', 'Heslo znovu:')
        ->setRequired(('Vyplňte prosím %label'))
        ->addRule($form::EQUAL, 'Hesla se neshodují', $form['password'])
        ->setOmitted();
        $form->addSubmit('send', 'Registrovat');
       //$form->onSuccess[] = [$this, 'formSucceeded']; //nutno dát tam, kde se bude používat, a vytvořit metodu

		return $form;
    }

}



?>