<?php
namespace App\Forms;
use Nette\Application\UI\Form;

final class LogInFactory {    
    /**
     * creates log in form
     *
     * @return Form return log in form
     */
    function create(): Form {
        $form = new Form;
        
        $form->addText('email', 'Email or username:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addPassword('password', 'Password:')
        ->setRequired(('Vyplňte prosím %label'));
        
        $form->addSubmit('send', 'Log in');
        //$form->onSuccess[] = [$this, 'formSucceeded']; //nutno dát tam, kde se bude používat, a vytvořit metodu

		return $form;
    }

}



?>