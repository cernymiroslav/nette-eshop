<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Forms;
use App\Model;
use Nette\Application\UI\Form;



/**
 * CartPresenter
 * Presenter for the cart template
 */
final class CartPresenter extends BasePresenter
{
    
    /**
     * default template render function
     *
     * @return void
     */
    function renderDefault(){
        $this->template->products = $this->getSession('cartSession')->get('cart');
        $this->template->addFunction('getProduct', function ($id) {
            return $this->productManager->getProduct($id);
        });
        
    }
    
    /**
     * action that sends new order based on cart session data
     *
     * @return void
     */
    function actionSendOrder(){
        $total = 0;
        $product_list = $this->getSession('cartSession')->get('cart');
        foreach ($product_list as $product) {
            $row = $this->productManager->getProduct($product->id);
            $total += $product->quantity*$row->product_price;
        }
        $this->dbManager->createOrder($this->user->getIdentity()->getId(), $total, $product_list);
        $this->getSession('cartSession')->remove('cart');
        $this->flashMessage("Objednávka úspěšně odeslána!");
        $this->redirect("Cart:");
    }




}


/**
 * Product
 * Class for product objects in cart session instance
 */
class Product
{
    public $id;
    public $quantity;

     /**
     * constructor for product objects
     *
     * @param  int $id id of the product
     * @param  int $quantity quantity of the product in cart
     * @return void
     */
    public function __construct($id, $quantity) {
        $this->id = $id;
        $this->quantity = $quantity;
      }
}


