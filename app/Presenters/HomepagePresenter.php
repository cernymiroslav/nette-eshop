<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Forms;
use App\Model;
use Nette\Application\UI\Form;


/**
 * HomepagePresenter
 * Presenter for the homepage template
 */
final class HomepagePresenter extends BasePresenter
{
    
    /**
     * constructor
     *
     * @return void
     */
    function __construct()
    {}
    
    /**
     * default template render function
     *
     * @return void
     */
    function renderDefault(){
        $products = $this->productManager->getProducts();
        $this->template->products = $products;
        bdump($products);
    }
    
    /**
     * function that renders template based on id of category
     * it fills products variable with products returned based on category id
     *
     * @param  int $category_id id of the selected category
     * @return void
     */
    function renderSelectedCategory($category_id){
        $products = $this->productManager->getProductsByCategoryId($category_id);
        $this->template->products = $products;
        $this->template->category = $this->categoryManager->getCategoryByCategoryID($category_id);
    }
    
    /**
     * function that logs user out
     *
     * @return void
     */
    function actionLogout(){
        $this->getUser()->logout();
        $this->redirect("Homepage:");
    }




}

