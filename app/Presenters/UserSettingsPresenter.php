<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use App\Forms;

/**
 * UserSettingsPresenter
 * Presenter for the user settings template
 */
final class UserSettingsPresenter extends BasePresenter
{

    private $changePasswordFactory;
    private $passwords;

    function __construct(Forms\ChangePasswordFactory $changePasswordFactory, Nette\Security\Passwords $passwords)
    {
        $this->changePasswordFactory = $changePasswordFactory;
        $this->passwords = $passwords;
    }

    function renderDefault(){
    }
    
    /**
     * function that creates change password form
     *
     * @return Form the form instance
     */
    protected function createComponentChangePasswordForm(): Form
	{
		$form = new Form;
		$form = $this->changePasswordFactory->create();
		$form->onSuccess[] = [$this, 'changePassword'];
		return $form;
	}
	
	/**
	 * function that's ran on submit of change password form
	 * verifies whether current password is correct, and if so, updates the password
     * 
	 * @param  Form $form the form instance
	 * @param  array $data array of data
	 * @return void
	 */
	public function changePassword(Form $form, $data): void
	{

        $row = $this->userManager->getUser($this->user->getIdentity()->name);
		if (!$this->passwords->verify($data->current_password, $row->password)) {
            $this->flashMessage("Zadané heslo není správné");
            $this->redirect('UserSettings:');
		}


        $this->userManager->updateUserPassword($this->user->getIdentity()->getId(), $this->passwords->hash($data->new_password));

        $this->flashMessage("Heslo bylo změněno");
        $this->redirect('UserSettings:');
	}
    

}

