<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Forms;
use App\Model;
use Nette\Application\UI\Form;


/**
 * LogInPresenter
 * Presenter for the log in template
 */
final class LogInPresenter extends BasePresenter
{
    private $logInFactory;
    
    /**
     * constructor
     *
     * @param  Forms\LogInFactory $logInFactory
     * @return void
     */
    function __construct(Forms\LogInFactory $logInFactory)
    {   
        $this->logInFactory = $logInFactory;
    }
    
    /**
     * default template render function
     *
     * @return void
     */
    function renderDefault(){

    }
        
    /**
     * function that creates log in form
     *
     * @return void
     */
    function createComponentLogIn(){
        $form = $this->logInFactory->create();
        $form->onSuccess[] = [$this, 'logIn'];
        return $form;
    }
        
    /**
     * function that's ran after log in form is submitted, tries to log user in
     * logs user in if successful, if unsuccesful, gives him error message
     *
	 * @param  Form $form the form instance
	 * @param  array $data array of data
     * @return void
     */
    function logIn(Form $form, $data): void
    {
        $user = $this->getUser();
        try {
            $user->login($data->email, $data->password);
            $this->flashMessage("Úspěšné přihlášení");
            $this->redirect('Homepage:');
        } catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage('Uživatelské jméno nebo heslo je nesprávné');
        }
        
    }



}

?>
