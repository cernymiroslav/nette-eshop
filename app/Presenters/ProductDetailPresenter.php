<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Forms;
use App\Model;
use Nette\Application\UI\Form;


/**
 * ProductDetailPresenter
 * Presenter for the product detail template
 */
final class ProductDetailPresenter extends BasePresenter
{
    
    /**
     * default template render function
     *
     * @param  int $id id of the product to be rendered
     * @return void
     */
    function renderDefault($id){
        bdump($id);
        $product = $this->productManager->getProduct($id);
        $this->template->product = $product;

        $this->template->addFunction('removeProduct', function ($product_id) {
            return $this->productManager->removeProduct($product_id);
        });
    }

    function __construct()
    {   
    }

}
