<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Controls\CartControl;
use App\Forms;
use App\Model;
use Nette\Application\UI\Form;

/**
 * BasePresenter
 * Base presenter that's to be extended by other presenters
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

	/** @var dbManager */
	public $dbManager;

	/** @var productManager */
	public $productManager;

	/** @var orderManager */
	public $orderManager;

	/** @var orderStatusManager */
	public $orderStatusManager;

	/** @var userManager */
	public $userManager;

	/** @var categoryManager */
	public $categoryManager;

	
	/** @var roleManager */
	public $roleManager;

	
	/**
	 * injectBase function
	 *
	 * @param  Model\DbManager $dbManager
	 * @param  Model\ProductManager $productManager
	 * @param  Model\OrderManager $orderManager
	 * @param  Model\OrderStatusManager $orderStatusManager
	 * @param  Model\UserManager $userManager
	 * @param  Model\CategoryManager $categoryManager
	 * @param  Model\RoleManager $roleManager
	 * @return void
	 */
	public function injectBase(Model\DbManager $dbManager, Model\ProductManager $productManager, Model\OrderManager $orderManager, Model\OrderStatusManager $orderStatusManager, Model\UserManager $userManager, Model\CategoryManager $categoryManager, Model\RoleManager $roleManager): void
	{
		$this->dbManager = $dbManager;
		$this->productManager = $productManager;
		$this->orderManager = $orderManager;
		$this->orderStatusManager = $orderStatusManager;
		$this->userManager = $userManager;
		$this->categoryManager = $categoryManager;
		$this->roleManager = $roleManager;


	}
	
	/**
	 * startup function
	 *
	 * @return void
	 */
	protected function startup(){
		parent::startup();
        $this->template->categories = $this->categoryManager->getCategories();
    }
		
	/**
	 * beforeRender function
	 *
	 * @return void
	 */
	function beforeRender(){
		$cartItems = $this->getSession()->getSection('cartSession')->get('cart');
		if ($cartItems) {
			$cartItemsCount = count($cartItems);
		} else $cartItemsCount = null;
		$this->template->numberOfItemsInCart = $cartItemsCount;
	}

	
	/**
	 * function that creates cart component
	 *
	 * @return CartControl $cart cart object
	 */
	protected function createComponentCart(): CartControl
	{
		$cart = new CartControl($this->getSession());		
		return $cart;
	}



}




