<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use App\Forms;

/**
 * OrderPresenter
 * Presenter for the order template
 */
final class OrderPresenter extends BasePresenter
{

    
    /**
     * startup function
     *
     * @return void
     */
    protected function startup() {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            $this->flashMessage("You have to be signed in!");
			$this->redirect('Homepage:', ['backlink' => $this->storeRequest()]);
		}


    }

    
    /**
     * default template render function
     *
     * @return void
     */
    function renderDefault(){
        $user_id = $this->user->getIdentity()->getId();
        $this->template->orders = $this->orderManager->getOrdersByUserId($user_id);
    }


}

