<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use App\Forms;

/**
 * AdministrationPresenter
 * Presenter for the administration template
 */
final class AdministrationPresenter extends BasePresenter
{

    private $newProductFactory;
    
    /**
     * startup function
     *
     * @return void
     */
    protected function startup() {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            $this->flashMessage("You have to be signed in!");
			$this->redirect('Homepage:', ['backlink' => $this->storeRequest()]);
		}

        if ($this->user->getIdentity()->getRoles()[0]!="admin"){
            $this->flashMessage("You have to be admin to access this page!");
			$this->redirect('Homepage:', ['backlink' => $this->storeRequest()]);
        }

    }
    
    /**
     * constructor
     *
     * @param  Forms\NewProductFormFactory $newProductFactory
     * @return void
     */
    function __construct(Forms\NewProductFormFactory $newProductFactory)
    {
        $this->newProductFactory = $newProductFactory;
    }
    
    /**
     * default template render function
     *
     * @return void
     */
    function renderDefault(){
    }
    
    /**
     * order template render function
     *
     * @return void
     */
    function renderOrders(){
        $this->template->orders = $this->orderManager->getOrders();

        $this->template->addFunction('getAvailableOrderStatus', function ($status_id) {
            return $this->orderStatusManager->getAvailableOrderStatus($status_id);
        });

    }
    
    /**
     * user template render function
     *
     * @return void
     */
    function renderUsers(){
        $this->template->users = $this->userManager->getUsers();

        $this->template->addFunction('getAvailableUserRoles', function ($role_id) {
            return $this->roleManager->getAvailableUserRoles($role_id);
        });

    }
    
    /**
     * product template render function
     *
     * @return void
     */
    function renderProducts(){
        $this->template->products = $this->productManager->getProducts();
    }
    
    /**
     * new product template render function
     *
     * @return void
     */
    function renderNewProduct(){

    }
    
    /**
     * new product form component create function
     *
     * @return Form the new product form
     */
    protected function createComponentNewProductForm(): Form
	{
		$form = new Form;
		$form = $this->newProductFactory->create($this->categoryManager->getCategories());
		$form->onSuccess[] = [$this, 'sendNewArticle'];
		return $form;
	}
	
	/**
	 * function that's ran on submit of new product function, sends data to new product database function
	 *
	 * @param  Form $form the form instance
	 * @param  array $data array of data
	 * @return void
	 */
	public function sendNewArticle(Form $form, $data): void
	{   
        bdump($data); 
        $this->productManager->newProduct($data->product_name, $data->product_price, $data->product_description, $data->amount_in_stock, $data->category_id);
        $this->flashMessage("Produkt byl úspěšně přidán do databáze");
        $this->redirect('Administration:newProduct');
        bdump($data); 
	}
    
    /**
     * action that changes user's status
     *
     * @param  int $userId id of the user to be changed
     * @param  int $newRoleId id of the new role
     * @return void
     */
    public function actionChangeUserStatus($userId, $newRoleId): void
	{   
        $this->userManager->changeUserRole($userId, $newRoleId);
        $this->flashMessage("Uživateli byla změněna práva.");
        $this->redirect('Administration:users');
	}
    
    /**
     * action that remove's user based on id
     *
     * @param  int $user_id id of the user to be removed
     * @return void
     */
    public function actionRemoveUser($user_id): void
	{   
        $this->userManager->removeUser($user_id);
        $this->flashMessage("Uživatel byl smazán.");
        $this->redirect('Administration:users');
	}
    
    /**
     * action that changes order's status
     *
     * @param  int $order_id id of order to be changed
     * @param  int $newStatusId new status id
     * @return void
     */
    public function actionChangeOrderStatus($order_id, $newStatusId): void
	{   
        $this->orderManager->changeOrderStatus($order_id, $newStatusId);
        $this->flashMessage("Status objednávky byl updatován.");
        $this->redirect('Administration:orders');
	}
    
    /**
     * action that removes order by id
     *
     * @param  int $order_id id of order to be removed
     * @return void
     */
    public function actionRemoveOrder($order_id): void
	{   
        $this->orderManager->removeOrder($order_id);
        $this->flashMessage("Objednávka byla smazána.");
        $this->redirect('Administration:orders');
	}

    
    /**
     * action that remove's product
     *
     * @param  int $product_id id of product to be removed
     * @return void
     */
    public function actionRemoveProduct($product_id): void
	{   
        $this->productManager->removeProduct($product_id);
        $this->flashMessage("Produkt byl smazán.");
        $this->redirect('Administration:products');
	}
    
    /**
     * creates stock form component
     *
     * @return Form stock change form
     */
    public function createComponentStockForm(): Form{
        $form = new Form;
        
        $form->addSelect('product_id', 'Product id:', $this->productManager->getProducts());

        $form->addInteger('amount_in_stock', 'Stock:')
        ->setRequired(('Vyplňte prosím %label'));

        $form->addSubmit('send', 'Update');
        $form->onSuccess[] = [$this, 'updateAmountInStock'];

		return $form;
    }

    
    /**
	 * function that's ran on submit of change product stock form, sends data  to update product stock 
     *
	 * @param  Form $form the form instance
	 * @param  array $data array of data
     * @return void
     */
    public function updateAmountInStock(Form $form, $data): void
	{    
        $this->productManager->updateProductStock($data->product_id, $data->amount_in_stock);
        $this->flashMessage("Produkt byl upraven");
        $this->redirect('Administration:products');
	}

}

