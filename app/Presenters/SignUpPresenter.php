<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Forms;
use App\Model;
use Nette\Application\UI\Form;


/**
 * SignUpPresenter
 * Presenter for the sign up template
 */
final class SignUpPresenter extends BasePresenter
{

    private $signUpFactory;
    private $passwords;
    
    /**
     * constructor
     *
     * @param  Forms\SignUpFactory $signUpFactory
     * @param  Nette\Security\Passwords $passwords
     * @return void
     */
    function __construct(Forms\SignUpFactory $signUpFactory, Nette\Security\Passwords $passwords)
    {   
        $this->signUpFactory = $signUpFactory;
        $this->passwords = $passwords;
    }
        
    /**
     * function that creates sing up form
     *
     * @return Form $form the form instance
     */
    function createComponentSignUp(): Form{
        $form = $this->signUpFactory->create();
        $form->onSuccess[] = [$this, 'signUp'];
        return $form;
    }
        
    /**
     * sign up function that's ran on submit of sign up form
     *
	 * @param  Form $form the form instance
	 * @param  array $data array of data
     * @return void
     */
    function signUp(Form $form, $data): void
    {
        $password_hash = $this->passwords->hash($data->password);
        $this->userManager->newUser($data->username, $password_hash, $data->firstname, $data->surname, $data->email);
        $this->flashMessage("Uživatel úspěšně zaregistrován, můžete se přihlásit!");
        $this->redirect("this");
    }
        
}

?>
