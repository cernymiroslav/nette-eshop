<?php

namespace App\Model;
use Nette;
use DateTime;
use Nette\Database\Table\Selection;


final class RoleManager extends BaseModel {

    /**
     * returns string name of the table that's to be worked with in the class
     *
     * @return string
     */
    public function getTable():string{
        return "roles";
    }

       
    /**
     * returns all user role records from database
     *
     * @return array array of user role record objects from database
     */
    function getUserRoles(){
        $user_roles = $this
                ->SelectTable()
                ->select('*')
                ->fetchall();
        return $user_roles;
    }
    
    /**
     * returns all roles except for the one in parameter
     *
     * @param  int $role_id role id not to be included in select
     * @return array array of user role record objects from database
     */
    function getAvailableUserRoles($role_id){
        $user_roles = $this
                ->SelectTable()
                ->select('*')
                ->where('role_id != ?', $role_id)
                ->fetchall();
        return $user_roles;
    }

}

    ?>