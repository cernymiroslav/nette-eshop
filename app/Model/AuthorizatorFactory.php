<?php
declare(strict_types=1);

namespace App\Model;
Use Nette\Database\Context;

class AuthorizatorFactory
{
        private $context;
        
        /**
         * create
         *
         * @return Nette\Security\Permission
         */
        public static function create(): \Nette\Security\Permission
        {
                $acl = new \Nette\Security\Permission;

                $acl->addRole('admin');
                $acl->addRole('user');

                $acl->addResource('backend');

                $acl->allow('admin', 'backend');
                $acl->deny('guest', 'backend');

                return $acl;
        }
}
?>