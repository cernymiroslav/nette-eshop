<?php

namespace App\Model;
use Nette;
use DateTime;
use App\Model;
use Nette\Security\SimpleIdentity;


class MyAuthenticator implements Nette\Security\Authenticator {
	private $userManager;
    private $dbManager;
    private $passwords;
        
    /**
     * construct function
     *
     * @param  Model\DbManager $dbManager $dbManager 
     * @param  Nette\Security\Passwords $passwords
     * @param  Model\UserManager $userManager
     * @return void
     */
    public function __construct(Model\DbManager $dbManager, Nette\Security\Passwords $passwords, Model\UserManager $userManager){
    $this->dbManager = $dbManager;
    $this->passwords = $passwords;
	$this->userManager = $userManager;
    } 
    
    /**
     * function checks whether user's name and password is correct, if so, returns SimpleIdentity instance
     *
     * @param  int $key id of user
     * @param  string $password password of the user
     * @return Nette\Security\IIdentity instance of user
     */
    public function authenticate(string $key, string $password): Nette\Security\IIdentity
	{
		$row = $this->userManager->getUser($key);
		if (!$row) {
			throw new Nette\Security\AuthenticationException('User not found.');
		}

		if (!$this->passwords->verify($password, $row->password)) {
			throw new Nette\Security\AuthenticationException('Invalid password.');
		}

		return new SimpleIdentity(
			$row->user_id,
			$row->ref('roles','role_id')->role_name,
			['name' => $row->username]
		);
	}
}



    ?>