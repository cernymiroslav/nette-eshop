<?php

namespace App\Model;
use Nette;
use DateTime;
use Nette\Database\Table\Selection;


final class UserManager extends BaseModel {

    /**
     * returns string name of the table that's to be worked with in the class
     *
     * @return string
     */
    public function getTable():string{
        return "users";
    }
    
    /**
     * get user info based on key - his email or username
     *
     * @param  string $key user's email or username
     * @return object user object, if there's such username or email in database
     */
    function getUser($key){
        $user = $this
                ->SelectTable()
                ->select('*')
                ->whereOr([
                    'email' => $key,
                    'username' => $key
                ])
                ->fetch();
        return $user;
    }
    
    /**
     * get database records of all users
     *
     * @return array array of user record objects from database
     */
    function getUsers(){
        $users = $this
                ->SelectTable()
                ->select('*')
                ->fetchall();
        return $users;
    }
    
    /**
     * remove user database record based on id
     *
     * @param  int $user_id id of the user to be removed
     * @return void
     */
    function removeUser($user_id){
        $this->Delete("user_id",$user_id);
    }

        
    /**
     * create new user record in database
     *
     * @param  string $username user's username
     * @param  string $password_hash hashed user's password
     * @param  string $firstname firstname of the user
     * @param  string $lastname lastname of the user
     * @param  string $email email of the user
     * @return void
     */
    function newUser($username, $password_hash, $firstname, $lastname, $email){
        $this->Insert([
            'username' => $username,
            'password' => $password_hash,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'role_id' => 2
        ]);
    }
    
    /**
     * change user's role
     *
     * @param  int $userId id of the user to be changed
     * @param  int $newRoleId role id of the new role
     * @return void
     */
    function changeUserRole($userId, $newRoleId){
        $this->Update("user_id",$userId,[
                'role_id' => $newRoleId
            ]);
    }
    
    
    /**
     * update user's password
     *
     * @param  int $userId id of the user to change password of
     * @param  string $newPassword hashed new password string to be updated in database
     * @return void
     */
    function updateUserPassword($userId, $newPassword){
            $this->Update("user_id",$userId,[
                'password' => $newPassword
            ]);
    }
    
    /**
     * get user based on his id
     *
     * @param  int $id id of the user
     * @return object object of the user
     */
    function getUserById($id){
        $user = $this
                ->SelectTable()
                ->select('*')
                ->whereOr([
                    'user_id' => $id,
                ])
                ->fetch();
        return $user;
    }

}

    ?>