<?php

namespace App\Model;
use Nette;
use DateTime;
use Nette\Database\Table\Selection;


final class OrderManager extends BaseModel {


    /**
     * returns string name of the table that's to be worked with in the class
     *
     * @return string
     */
    public function getTable():string{
        return "orders";
    }
    
    /**
     * function returns all orders from database
     *
     * @return array array of order records from database
     */
    function getOrders(){
        $orders = $this
                ->SelectTable()
                ->select('*')
                ->fetchall();
        return $orders;
    }
    
    /**
     * function returns orders created by user by id
     *
     * @param  int $user_id id of the user
     * @return array array of orders made by the user
     */
    function getOrdersByUserId($user_id){
        $orders = $this
                ->SelectTable()
                ->select('*')
                ->where('user_id', $user_id)
                ->fetchall();
        return $orders;
    }
    
    /**
     * function returns order record based on it's id
     *
     * @param  int $order_id
     * @return object order record from database
     */
    function getOrder($order_id){
        $order = $this
                ->SelectTable()
                ->select('*')
                ->where('order_id', $order_id)
                ->fetch();
        return $order;
    }
    
    /**
     * changes status of order by ids of order and new status id
     *
     * @param  int $order_id id of the order to be changed
     * @param  int $newStatusId id of status to be set to the order
     * @return void
     */
    function changeOrderStatus($order_id, $newStatusId){
        $this
        ->SelectTable()
        ->where('order_id', $order_id)
        ->update([
            'status_id' => $newStatusId
        ]);
    }
    
    /**
     * removes order based on it's id
     * also cleanups order_content table from contents of the order to be removed 
     * @param  int $order_id id of the order to be removed
     * @return void
     */
    function removeOrder($order_id){
        $this->Delete('order_id' , $order_id);
        $this->SelectTableByName('order_content')->where('order_id', $order_id)->delete();

    }

}

    ?>