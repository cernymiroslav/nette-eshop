<?php

namespace App\Model;
use Nette;



class MyAuthorizator implements Nette\Security\Authorizator
{	
	/**
	 * function checks whether user's able to use resource
	 *
	 * @param  string $role user's role
	 * @param  string $resource resource's name
	 * @param  string $operation operation's name
	 * @return bool true if user is allowed, false if he isn't
	 */
	public function isAllowed($role, $resource, $operation): bool
	{
		if ($role === 'admin') {
			return true;
		}
		if ($role === 'user' && $resource === 'orders') {
			return true;
		}

		return false;
	}
}




    ?>