<?php

namespace App\Model;
use Nette;
use DateTime;
use Nette\Database\Table\Selection;


final class ProductManager extends BaseModel {

    /**
     * returns string name of the table that's to be worked with in the class
     *
     * @return string
     */
    public function getTable():string{
        return "products";
    }

    
    /**
     * returns all product records from database
     *
     * @return array array of product record objects from database
     */
    function getProducts(){
        $products = $this
                    ->SelectTable()
                    ->select('*')
                    ->fetchAll();
        return $products;
    }
    
    /**
     * returns products in selected category
     *
     * @param  int $category_id id of the category to get products by
     * @return array array of product records objects in selected category
     */
    function getProductsByCategoryId($category_id){
        $products = $this
                    ->SelectTable()
                    ->select('*')
                    ->where('category_id', $category_id)
                    ->fetchAll();
        return $products;
    }
    
    /**
     * get product record by id
     *
     * @param  int $id id of the product to be returned
     * @return object object of product record from database
     */
    function getProduct($id){
        $product = $this
            ->SelectTable()
            ->select('*')
            ->where('product_id', $id)
            ->fetch();
        return $product;
    }
    
    /**
     * function creates new product record in database
     *
     * @param  string $product_name name of the product
     * @param  float $product_price price of the product
     * @param  string $product_description description of the product
     * @param  int $amount_in_stock the amount of product in stock
     * @param  int $category_id category id of product's category
     * @return void
     */
    function newProduct($product_name, $product_price, $product_description, $amount_in_stock, $category_id){
        $this->Insert([
            'product_name' => $product_name,
            'product_price' => $product_price,
            'product_description' => $product_description,
            'amount_in_stock' => $amount_in_stock,
            'category_id' => $category_id
        ]);
    }
    
    /**
     * update the stock of product
     *
     * @param  int $product_id product id to be updated
     * @param  int $amount_in_stock the new number of stock 
     * @return void
     */
    function updateProductStock($product_id, $amount_in_stock){
        $this->Update('product_id', $product_id, [
         'amount_in_stock' => $amount_in_stock   
        ]);
    }
    
    /**
     * remove product by id
     *
     * @param  int $product_id id of the product
     * @return void
     */
    function removeProduct($product_id){
        $this->Delete("product_id",$product_id);
    }


}

    ?>