<?php

namespace App\Model;
use Nette;
use DateTime;


final class DbManager {
    private $database;
        
    /**
     * construct function
     *
     * @param  Nette\Database\Explorer $database
     * @return void
     */
    public function __construct(Nette\Database\Explorer $database){
        $this->database = $database;
    } 

    /**
     * function creates order record in database and multiple order_content records based on content of the order
     *
     * @param  int $userId id of the user creating order
     * @param  float $total total price of order
     * @param  array $products array of product objects from cart
     * @return void
     */
    //nelze přesunout(přístup do jiné tabulky, potřeba odpovědi při insertu)    
    function createOrder($userId, $total, $products){
        $row = $this->database->table('orders')->insert([
            'date' => new DateTime,
            'total' => $total, 
            'user_id' => $userId, 
            'status_id' => 1
        ]);

        foreach($products as $product) {
            $this->database
            ->table('order_content')
            ->insert([
                'order_id' => $row->order_id,
                'product_id' => $product->id,
                'order_amount' => $product->quantity

            ]);
        }


    }

}

    ?>