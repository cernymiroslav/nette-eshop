<?php

namespace App\Model;
use Nette;
use DateTime;
use Nette\Database\Table\Selection;


abstract class BaseModel {
    private $database;

    private $tableName;
    
    public function __construct(Nette\Database\Explorer $database){

        // přístup k DB
        $this->database = $database;

        // Jméno tabulky, se kterou budeme pracovat
        $this->tableName = $this->getTable();
    } 

    /**
     * funkce pro vkládání dat do tabulky
     * @param $data array pole klíč => hodnota
     */
    public function Insert($data) : void {
        $this->database
            ->table($this->tableName)
            ->insert($data);
    }

    /**
     * funkce pro úpravu dat v tabulce
     * @param $id id záznamu
     * @param $data array pole klíč => hodnota
     */
    public function Update($id_column_name, int $id, $data) : void {
        $this->database
            ->table($this->tableName)
            ->where($id_column_name, $id)
            ->update($data);
    }
    
    /**
     * function that selects table in database
     *
     * @return Selection the db selection
     */
    function Select() : Selection {
        return $this->database
                ->table($this->tableName);
    }
    
    /**
     * function that selects table in database
     *
     * @return Selection the db selection
     */
    function SelectTable() : Selection {
        return $this->database
                ->table($this->tableName);
    }
    
    /**
     * function deletes column based on id 
     *
     * @param  mixed $id_column_name the name of the id column in database table
     * @param  mixed $id the id of column to remove
     * @return void
     */
    function Delete($id_column_name, $id) : void {
        $this->database
            ->table($this->tableName)
            ->where($id_column_name, $id)
            ->delete();
    }
    
    /**
     * function returns selection of specified table name
     *
     * @param  string $tableName name of the table to be selected
     * @return Selection the tableName selection
     */
    function SelectTableByName($tableName): Selection {
        return $this->database
                ->table($tableName);
    }
    
    /**
     * getTable abstract function that has to be implemented in children
     *
     * @return void
     */
    abstract public function getTable();





}

    ?>