<?php

namespace App\Model;
use Nette;
use DateTime;
use Nette\Database\Table\Selection;


final class OrderStatusManager extends BaseModel {

    /**
     * returns string name of the table that's to be worked with in the class
     *
     * @return string
     */
    public function getTable():string{
        return "status_list";
    }
    
    /**
     * get available order statuses except the one in parameter
     *
     * @param  int $status_id id of the status not to be included in return
     * @return array array of available order statuses records
     */
    function getAvailableOrderStatus($status_id){
        $orderStatuses = $this
                ->Select()
                ->select('*')
                ->where('status_id != ?', $status_id)
                ->fetchall();
        return $orderStatuses;
    }
    
    /**
     * returns all order statuses
     *
     * @return array array of order status records from database
     */
    function getOrderStatuses(){
        $orderStatuses = $this
                ->Select()
                ->select('*')
                ->fetchall();
        return $orderStatuses;
    }

}

    ?>