<?php

namespace App\Model;
use Nette;
use DateTime;
use Nette\Database\Table\Selection;


final class CategoryManager extends BaseModel {
    
    
    /**
     * returns string name of the table that's to be worked with in the class
     *
     * @return string
     */
    public function getTable():string{
        return "categories";
    }

       
    /**
     * function returns array of category objects from database
     *
     * @return array array of category objects
     */
    function getCategories(){
        $categories = $this
                ->SelectTable()
                ->select('*')
                ->fetchAll();
        return $categories;
    }
    
    /**
     * function returns category from database based on it's id
     *
     * @param  int $category_id id of the category to be returned
     * @return object object of category record from database
     */
    function getCategoryByCategoryID($category_id){
        $categories = $this
                ->SelectTable()
                ->select('*')
                ->where('category_id', $category_id)
                ->fetch();
        return $categories;
    }

}

    ?>