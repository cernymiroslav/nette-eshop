<?php
namespace App\Controls;
use Nette\Application\UI\Control;
use Nette\Http\Session;

/**
 * CartControl 
 * Class controls cart
 * 
 * @author Černý Miroslav
 */
class CartControl extends Control
{

    private $session;
    
    /**
     * construct method
     *
     * @param  mixed $session session instance
     * @return void
     */
    function __construct(Session $session)
	{
		$this->session = $session;
	}
    
    /**
     * render method, dictates what file to render
     *
     * @return void
     */
    public function render(): void {
        $this->template->render(__DIR__ . '/poll.latte');
    }
    
    /**
     * Function adds an item to the session instance of cart based on it's id 
     *
     * @param  int $itemId the database id of the item to add to cart
     * @return void
     */
    public function handleAddItem($itemId): void {
        $section = $this->session->getSection('cartSession');
        $key = false;

        $cartValues = $section->get('cart');

        if ($cartValues==null) {
            $section->set('cart', []);
        } 
        else if ($cartValues!=[]) {
            $key = array_search($itemId, array_column($cartValues, 'id'));
            bdump($cartValues);
            bdump($itemId);
            bdump($key);
        }

        $cartValues = $section->get('cart');

        if ($key===false) {
            array_push($cartValues, new Product($itemId, 1));
        }
        else {
                $productRecord = $cartValues[$key];
                $productRecord->quantity += 1;
                array_replace($cartValues, array($key=>$productRecord));
            }

        bdump($cartValues);

        $section->set('cart', $cartValues);
        bdump($section->get('cart'));
    }
    
    /**
     * Function removes item from session instance of cart by it's id
     *
     * @param  int $itemId id of the item to remove from the cart
     * @return void
     */
    public function handleRemoveItem($itemId): void {
        $section = $this->session->getSection('cartSession');
        $key = false;
        $cartValues = $section->get('cart');
        bdump($cartValues);

        $key = array_search($itemId, array_column($cartValues, 'id'));
        if ($key===false) {
        } else {
            bdump($key);
            unset($cartValues[$key]);
            $cartValues = array_values($cartValues);
        }

        $section->set('cart', $cartValues);
    }
    
    /**
     * Function cleans the cart session isntance
     *
     * @return void
     */
    public function handleCleanCart(): void {
        $section = $this->session->getSection('cartSession');
        $section->set('cart', null);
        $this->flashMessage("Cart cleared!");
        $this->redirect('this');
    }
}

/**
 * Product
 * Class for product objects in cart session instance
 */
class Product
{
    public $id;
    public $quantity;
    
    /**
     * constructor for product objects
     *
     * @param  int $id id of the product
     * @param  int $quantity quantity of the product in cart
     * @return void
     */
    public function __construct($id, $quantity) {
        $this->id = $id;
        $this->quantity = $quantity;
      }
}

?>